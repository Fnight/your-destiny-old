import re

output = '''/*
========================
=====OOOOOO=O=====O=====
=====O======OO====O=====
=====O======O=O===O=====
=====OOO====O=====O=====
=====O======O===O=O=====
=====O======O====OO=====
=====O======O=====O=====
========================
=====OOOOOOOOOOOOOO=====
========================
=========PSUMAP=========
====COPYRIGHT FNIGHT====
========================
*/

var scenario = [\n'''

with open('YourDestiny.sm') as f:
	for fragment in re.split(r':: ', f.read().replace('"','\\"')):
		small_fragments = list(filter(None, re.split(r'\n', fragment)))
		if len(small_fragments) > 2:
			part_id = small_fragments[0][:small_fragments[0].index('[')]
			part_left = list(filter(lambda x: 'Проведи влево' in x, small_fragments))
			part_right = list(filter(lambda x: 'Проведи вправо' in x, small_fragments))
			if len(part_left) == 0:
				part_left = ''
			else:
				part_left = part_left[0]
			if len(part_right) == 0:
				part_right = ''
			else:
				part_right = part_right[0] 
			output += '''\t{{
			id : "{0}",
			description : "{1}",
			left_description : "{2}",
			right_description : "{3}",
			left_id : "{4}",
			right_id : "{5}"}},\n'''.format(
				part_id, 
				small_fragments[1],
				'' if part_left == '' else part_left[2:part_left.index('|')],
				'' if part_right == '' else part_right[2:part_right.index('|')],
				'' if part_left == '' else part_left[part_left.index('|') + 1:-2],
				'' if part_right == '' else part_right[part_right.index('|') + 1:-2]
				)

output += '];'

with open('app/www/js/scenario.js', 'w', encoding='utf-8') as f:
	f.write(output.replace('\t\t\t', '\t\t'))
	f.close()